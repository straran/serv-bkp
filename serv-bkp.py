import os
import ConfigParser
import sys
from datetime import datetime
import subprocess

#parametro do arquivo de configuracao
configfile = str(sys.argv[1])

#lendo arquivo de config
config = ConfigParser.ConfigParser()
config.read(configfile)

#setando variaveis
dirbkp =  config.get('DEFAULT','tempdir')
dia = str(datetime.now().strftime('%Y-%m-%d-%H-%M'))
dbuser = config.get('DEFAULT', 'dbuser')
dbpass = config.get('DEFAULT', 'dbpass')

#pegar ID para pasta de upload
folder = config.get('DEFAULT','google_folder')
pasta = subprocess.Popen('./drive list -t ' + folder + ' | grep -w ' + folder ,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
pasta = pasta.stdout.readline().split()[0]
print pasta

#loop para todas os users
for dados in config.sections():
	# ~ print dados
	#variaveis
	user = config.get(dados, 'user') 
	dir =  config.get(dados,'dir')
	base = config.get(dados,'db')
	# ~ base_passwd= config.get(dados,'dbpass')
	
	#criando diretorio para bk
	if dirbkp[-1] != "/":
		dirbkp = dirbkp + "/"

	bkpdir = dirbkp + dados
	# ~ print('mkdir -p '+ bkpdir)
	print "Criando Diretorio BKP"
	os.system('mkdir -p '+ bkpdir)

	#copiando raiz do user para pasta bkp
	# ~ print('cp -r '+ dir + '/* ' + bkpdir)
	print "Copiando arquivos"
	os.system('cp -r '+ dir + '/* ' + bkpdir)

	#dump do banco de dados
	# ~ print('mysqldump ' + base + ' -u' + dbuser + ' -p' + dbpass + ' > ' + bkpdir + '/' + base + '.sql')
	print "BKP de Mysql"
	os.system('mysqldump ' + base + ' -u' + dbuser + ' -p' + dbpass + ' > ' + bkpdir + '/' + base + '.sql')

	#compactando diretorio de bkp do user
	nometar = bkpdir + dia + '.tar '
	# ~ print('tar -cvf ' + nometar + bkpdir)
	print "Compactando arquivos"
	os.system('tar -cvf ' + nometar + bkpdir)

	#fazendo upload para google drive
	# ~ print('./drive upload -f ' + nometar + '-p ' + pasta)
	print "Enviando para Drive"
	os.system('./drive upload -f ' + nometar + '-p ' + pasta)

	#apagando diretorio de bkp e zip
	print "Removendo arquivos de BKP"
	os.system('rm -rf ' + bkpdir)
	os.system('rm -rf ' + nometar)
	# ~ os.system('rm -rf ' + bkpdir)

